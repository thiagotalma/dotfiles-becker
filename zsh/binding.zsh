bindkey '\e[1~'     beginning-of-line  # Linux console
bindkey '\e[H'      beginning-of-line  # xterm
bindkey '\eOH'      beginning-of-line  # gnome-terminal
bindkey '^[[5D'     beginning-of-line
bindkey '\e[4~'     end-of-line        # Linux console
bindkey '\e[F'      end-of-line        # xterm
bindkey '\eOF'      end-of-line        # gnome-terminal
bindkey '^[[5C'     end-of-line

bindkey '\e[3~'     delete-char        # Linux console, xterm, gnome-terminal
bindkey '^[[3~'     delete-char
bindkey '^?'        backward-delete-char

bindkey '\e[1;5D'   backward-word
bindkey '\e[5D'     backward-word
bindkey '\e\e[D'    backward-word
bindkey '^[^[[D'    backward-word
bindkey '^[[1;5D'   backward-word
bindkey '^[[5D'     backward-word

bindkey '^[^[[C'    forward-word
bindkey '^[[1;5C'   forward-word
bindkey '^[[5C'     forward-word
bindkey '\e[1;5C'   forward-word
bindkey '\e[5C'     forward-word
bindkey '\e\e[C'    forward-word


bindkey '^[[A'      history-substring-search-up
bindkey '^[[B'      history-substring-search-down
