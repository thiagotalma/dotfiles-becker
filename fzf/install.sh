#!/bin/sh

if [ -d "$HOME/.fzf/.git" ]; then
    git --work-tree=$HOME/.fzf --git-dir=$HOME/.fzf/.git pull
else
    git clone --depth 1 https://github.com/junegunn/fzf.git $HOME/.fzf
fi

bash $HOME/.fzf/install --bin >/tmp/install-fzf.log 2>&1 || true
