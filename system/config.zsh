#!/bin/sh

# Do not overwrite files when redirecting using ">". Note that you can still override this with ">|"
set -o noclobber
